from flask import Flask
import os

app = Flask(__name__)

@app.route("/")
def skill():
    message = "{name} la dong vat"
    return message.format(name=os.getenv("NAME", "Meo"))
    
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=2563)
